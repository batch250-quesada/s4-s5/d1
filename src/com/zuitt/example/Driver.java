package com.zuitt.example;

public class Driver {

    // properties/variables
    private String name;

    // constructor
    public Driver() {};
    public Driver(String name) {
        this.name = name;
    }

    // getter and setter
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
