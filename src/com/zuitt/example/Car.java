package com.zuitt.example;

public class Car {
        //// Access Modifier ////
        // There are used to restrict the scope of a class, constructor, variable, method or data member

        // 4 Access Modifiers
            // default (no keyword)
            // private
            // protected
            // public

        /// Class creation
        // 4 parts

        // 1. Properties - refers to characteristics of an object; aka variable

        private String name;
        private String brand;
        private int yearOfMake;

        // makes additional component of a car
        private Driver driver;

        // 2. Constructor - used to create/instantiate an object.
            // a. empty constructor - creates object that does not have any arguments or parameters; also referred as default constructor
            public Car() {
                // set a default value upon instantiation
                this.yearOfMake = 1999;

                // whenever a new car is created, it will have a driver named "Chrissy"
                this.driver = new Driver("Chrissy");
            };

            // b. parameterized constructor - creates an object with arguments or parameters
            public Car(String name, String brand, int yearOfMake) {
                this.name = name;
                this.brand = brand;
                this.yearOfMake = yearOfMake;
                this.driver = new Driver("Chrissy");
            };

        // 3. Getters and Setters - get and set the values of each property of an object.
            // Getters - retrieves the value of instantiated object
            public String getName() {
                return this.name;
            }

            public String getBrand() {
                return this.brand;
            }

            public int getYearOfMake() {
                return this.yearOfMake;
            }

            public String getDriverName() {
                return this.driver.getName();
            }

            // Setters - used to change the default value of an instantiated object
            public void setName(String name) {
                this.name = name;
            }

            public void setBrand(String brand) {
                this.brand = brand;
            }

            public void setYearOfMake(int yearOfMake) {
                // can also be modified to add validation
                if (yearOfMake <= 2022) {
                  this.yearOfMake = yearOfMake;
                }
            }

            public void setDriver(String driver) {
                // this will invoke the setName() method of the Driver class
                this.driver.setName(driver);
            }

        // 4. Methods - function that an object can perform (optional)
        public void drive() {
            System.out.println("This car is running.");
        }
}
