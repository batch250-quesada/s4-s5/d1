package com.zuitt.example;

public class Person implements Actions, Greetings {

    @Override
    public void sleep() {
        System.out.println("@SLEEP: sleeping");
    }

    @Override
    public void run() {
        System.out.println("@RUN: running");
    }

    @Override
    public void morningGreet() {
        System.out.println("@MORNING: good morning baby");
    }

    @Override
    public void holidayGreet() {
        System.out.println("@HOLIDAY: annyeong yeobo");
    }
}
