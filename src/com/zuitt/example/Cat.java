package com.zuitt.example;

// child class of Animal.java
    // "extends" keyword is used to inherit the properties and methods of the parent class
public class Cat extends Animal {

    // properties
    private String breed;

    // constructor
    public Cat() {
        // to have direct access with the original constructor
        super();
        this.breed = "Persian";
    };

    public Cat(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    // getter and setter
    public String getBreed() {
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    // method
    public void speak() {
        System.out.println("@SPEAK: Meowww");
    }

    public void call() {
        super.call();
        System.out.println("@CALL: Hello, I am " + this.name);
    }
}
