package com.zuitt.example;

public interface Actions {

    void sleep();

    void run();

}
