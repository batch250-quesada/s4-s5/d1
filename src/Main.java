import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
        //// OOP ////
        // OOP stands for "Object-Oriented Programming".
        // OOP is a programming model that allows developers to design software around data or objects, rather than function and logic.

        // OOP Concepts
        // Object - abstract idea that represents something in the real world.
            // Example: The concept of a dog
        // Class - representation of the object using code.
            // Example: Writing a code that would describe a dog.
        // Instance - unique copy of the idea, made "physical"
            // Example: Instantiating a dog named Spot from the dog class.

        // Objects
        // States and Attributes-what is the idea about?
        // Behaviors-what can idea do?
        // Example: A person has attributes like name, age, height, and weight. And a person can eat, sleep, and speak.

        //Four Pillars of OOp
        // 1. Encapsulation
            // a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
            // "data hiding" - the variables of a class will be hidden from other classes, and can be accessed only through the methods of the current class.
            //variables/properties as private.
            // provide a public setter and getter function.

            Car myCar = new Car();
            myCar.drive();

            // Assign the property of myCar using the setter methods;
            myCar.setName("Volvosaur");
            myCar.setBrand("Volvo");
            myCar.setYearOfMake(2099);

            // view the properties of myCar using the getter methods:
            System.out.println("Car name: " + myCar.getName());
            System.out.println("Car brand: " + myCar.getBrand());
            System.out.println("Car year of make: " + myCar.getYearOfMake());

            // myCar.name; - error since name has private access

        // [Subtopic] Composition and Inheritance
            // Both concepts promotes code reuse through different approach
            //"Inheritance" allows modelling an object that is a subset of another objects.
            // It defines “is a relationship”.
            // To design a class on what it is.

            //"Composition" allows modelling objects that are made up of other objects.
            // both entities are dependent on each other
            // composed object cannot exist without the other entity.
            // It defines “has a relationship”.
            // To design a class on what it does.
            // Example:
            // A car is a vehicle - inheritance
            // A car has a driver - composition

            System.out.println("Driver name: " + myCar.getDriverName());

            myCar.setDriver("Krissy");
            System.out.println("Driver name: " + myCar.getDriverName());

        ///// 2. Inheritance /////
        // Can be defined as the process where one class acquires the properties and methods of another class.
        // With the use of inheritance the information is made manageable in hierarchical order.

        Cat myPet = new Cat();
        myPet.setName("Thomas");
        myPet.setColor("red");
        myPet.speak();
        myPet.call();

        System.out.println(myPet.getName() + " is my " + myPet.getColor() + " " + myPet.getBreed() + " cat." );

        //3. Abstraction
            // is a process where all the logic and complexity are hidden from the user.

            //Interfaces
            // This is used to achieve total abstraction.
            // Creating Abstract classes doesn't support "multiple inheritance", but it can achieve with interfaces.
            // act as "contracts" wherein a class implements the interface should have the methods that the interfaces has defined in the class.

            Person child = new Person();
            child.sleep();
            child.run();

            Person baby = new Person();
            baby.morningGreet();
            baby.holidayGreet();

        //4. Polymorphism
            // Derived from greek word: poly means "many" and morph means "forms".
            // In short "many forms"
            // this is usually done by function/method overloading.
            //two main types of polymorphism
            // Static or compile time polymorphism
            // methods with the same name, but they have different data types and a different number of arguments.

            // Dynamic or run-time polymorphism
            // Function is overridden by replacing the definition of the method in the parent class in th e child class.

            StaticPoly myAddition = new StaticPoly();

            // original method
            System.out.println(myAddition.addition(5, 6));

            // based on adding another argument
            System.out.println(myAddition.addition(5, 6, 7));

            // based on changing data types
            System.out.println(myAddition.addition(5.5d, 6.6d));
    }
}